#include "pigpio/pigpio.hpp"

#include <pigpiod_if2.h>

namespace pigpio
{

Pigpio::Pigpio()
  : rclcpp::Node{"pigpio"}
{
  pi_ = pigpio_start(NULL, NULL);
  if (pi_ < 0) {
    RCLCPP_ERROR(get_logger(), "Failed starting PIGPIO: %d", pi_);
    // TODO (sgvd): exit gracefully
    return;
  }
  RCLCPP_INFO(get_logger(), "Started PIGPIO: %d", pi_);

  // GET MODE
  get_mode_srv_ = create_service<pigpio_msgs::srv::GetMode>(
    "get_mode",
    [this](pigpio_msgs::srv::GetMode::Request::SharedPtr req, pigpio_msgs::srv::GetMode::Response::SharedPtr res) {
      auto result = get_mode(pi_, req->gpio);
      if (!check_result(result, "get mode")) {
        return false;
      }
      res->mode.data = result;
      return true;
    }
    );

  // SET MODE
  set_mode_sub_ = create_subscription<pigpio_msgs::msg::SetMode>(
    "set_mode",
    rclcpp::SystemDefaultsQoS(),
    [this](pigpio_msgs::msg::SetMode::UniquePtr msg) {
      auto result = set_mode(pi_, msg->gpio, msg->mode.data);
      check_result(result, "set mode");
    }
    );

  // READ
  read_srv_ = create_service<pigpio_msgs::srv::Read>(
    "read",
    [this](pigpio_msgs::srv::Read::Request::SharedPtr req, pigpio_msgs::srv::Read::Response::SharedPtr res) {
      auto result = gpio_read(pi_, req->gpio);
      if (!check_result(result, "read")) {
        return false;
      }
      res->level = result;
      return true;
    }
    );

  // WRITE
  write_sub_ = create_subscription<pigpio_msgs::msg::Write>(
    "write",
    rclcpp::SystemDefaultsQoS(),
    [this](pigpio_msgs::msg::Write::UniquePtr msg) {
      auto result = gpio_write(pi_, msg->gpio, msg->level);
      check_result(result, "write");
    }
    );
}

Pigpio::~Pigpio()
{
  if (pi_ >= 0) {
    pigpio_stop(pi_);
  }
}

bool Pigpio::check_result(int result, std::string description) {
  // Check for some basic error results
  switch (result) {
  case 0:
    // 0 is never bad
    return true;
  case PI_BAD_GPIO:
    RCLCPP_ERROR(get_logger(), "Failed %s: BAD GPIO", description.c_str());
    return false;
  case PI_BAD_MODE:
    RCLCPP_ERROR(get_logger(), "Failed %s: BAD MODE", description.c_str());
    return false;
  case PI_BAD_LEVEL:
    RCLCPP_ERROR(get_logger(), "Failed %s: BAD LEVEL", description.c_str());
    return false;
  case PI_NOT_PERMITTED:
    RCLCPP_ERROR(get_logger(), "Failed %s: NOT PERMITTED", description.c_str());
    return false;
  default:
    break;
  }

  // Fall back to general 'negative = bad'
  if (result < 0) {
    RCLCPP_ERROR(get_logger(), "Failed %s", description.c_str());
    return false;
  }

  // Positive result
  return true;
}
  
}  // namespace pigpio
