// Copyright 2020 Sander G. van Dijk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef PIGPIO__PIGPIO_HPP_
#define PIGPIO__PIGPIO_HPP_

#include "pigpio/visibility_control.h"

#include <rclcpp/rclcpp.hpp>
#include <pigpio_msgs/msg/set_mode.hpp>
#include <pigpio_msgs/msg/write.hpp>
#include <pigpio_msgs/srv/get_mode.hpp>
#include <pigpio_msgs/srv/read.hpp>

namespace pigpio
{

class Pigpio : public rclcpp::Node
{
public:
  Pigpio();

  virtual ~Pigpio();

private:
  int pi_;

  rclcpp::Service<pigpio_msgs::srv::GetMode>::SharedPtr get_mode_srv_;
  rclcpp::Subscription<pigpio_msgs::msg::SetMode>::SharedPtr set_mode_sub_;

  rclcpp::Service<pigpio_msgs::srv::Read>::SharedPtr read_srv_;
  rclcpp::Subscription<pigpio_msgs::msg::Write>::SharedPtr write_sub_;

  bool check_result(int result, std::string description);
};

}  // namespace pigpio

#endif  // PIGPIO__PIGPIO_HPP_
