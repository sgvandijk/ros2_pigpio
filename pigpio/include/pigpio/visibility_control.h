#ifndef PIGPIO__VISIBILITY_CONTROL_H_
#define PIGPIO__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define PIGPIO_EXPORT __attribute__ ((dllexport))
    #define PIGPIO_IMPORT __attribute__ ((dllimport))
  #else
    #define PIGPIO_EXPORT __declspec(dllexport)
    #define PIGPIO_IMPORT __declspec(dllimport)
  #endif
  #ifdef PIGPIO_BUILDING_LIBRARY
    #define PIGPIO_PUBLIC PIGPIO_EXPORT
  #else
    #define PIGPIO_PUBLIC PIGPIO_IMPORT
  #endif
  #define PIGPIO_PUBLIC_TYPE PIGPIO_PUBLIC
  #define PIGPIO_LOCAL
#else
  #define PIGPIO_EXPORT __attribute__ ((visibility("default")))
  #define PIGPIO_IMPORT
  #if __GNUC__ >= 4
    #define PIGPIO_PUBLIC __attribute__ ((visibility("default")))
    #define PIGPIO_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define PIGPIO_PUBLIC
    #define PIGPIO_LOCAL
  #endif
  #define PIGPIO_PUBLIC_TYPE
#endif

#endif  // PIGPIO__VISIBILITY_CONTROL_H_
